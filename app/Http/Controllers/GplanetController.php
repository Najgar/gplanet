<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Controllers\Controller;

class GplanetController extends Controller
{
    private function adjustDays($days){
        $days = explode(',',$days);
        $days[0] = substr($days[0],1,strlen($days[0]));
        $days[count($days)-1] = substr($days[count($days)-1],0,strlen($days[count($days)-2]));
        $index = 0;
        foreach ($days as $day){
            $days[$index] = ($day - 1) % 7;
            $index++;
        }

        sort($days);
        return $days;
    }
    /**
     * Show the profile for the given user.
     *
     * @param  int  $id
     * @return Response
     */
    public function index()
    {
        //echo "hi";
        if (isset($_GET['date']) && isset($_GET['days']) && isset($_GET['tofinish'])){
            $days = $this->adjustDays($_GET['days']);
            $dayofweek = date('w', strtotime($_GET['date']));

            $dates = array();
            $diff = array();
            foreach ($days as $day){
                if ($dayofweek > $day){
                    $temp = $dayofweek - $day;
                    $diff[count($diff)] = 7 - $temp;
                }

                else{
                    $diff[count($diff)] = $day - $dayofweek;
                }
            }

            sort($diff);

            $rolling_week = -7;
            $days_to_finish_work = 30 * $_GET['tofinish'];
            $days_taken = 0;
            $cutter_days = count($diff);

            while($days_taken != $days_to_finish_work){
                if ($days_taken%$cutter_days == 0){
                    $rolling_week = $rolling_week + 7;
                }
                //$dates[count($dates)] = $diff[$days_taken%$cutter_days] + $rolling_week;
                $temp_date = strtotime("+".$diff[$days_taken%$cutter_days] + $rolling_week." days", strtotime($_GET['date']));
                $dates[count($dates)] = date("Y-m-d", $temp_date);
                $days_taken++;
            }

            $response = array();
            $response['Sessions'] = $dates;
            echo json_encode($response);
        }
    }
}